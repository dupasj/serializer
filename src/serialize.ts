import EXPORT from "./type/export";
import TYPE from "./type/type";
import Serialized from "./type/serialized";

const serialise = (entry: any,type: EXPORT = EXPORT.BASE64): string|Serialized => {
    const refs: any[] = [];
    const computeRefs = (obj: any) => {
        if (refs.includes(obj)){
            return;
        }

        if (Array.isArray(obj)){
            refs.push(obj);
            obj.forEach(computeRefs);
            return;
        }

        if (typeof obj === "object" && obj !== null){
            refs.push(obj);
            for(const key in obj){
                computeRefs(obj[key]);
            }
            return;
        }

        refs.push(obj);
    };

    computeRefs(entry);

    const data = refs.map((ref) => {
        if (Array.isArray(ref)){
            return [
                TYPE.ARRAY,
                ref.map((item) => {
                    return refs.indexOf(item);
                })
            ]
        }

        if (typeof ref === "undefined"){
            return [TYPE.UNDEFINED];
        }

        if (typeof ref === "object" && ref !== null){
            const obj: {[key: string|number]: number} = {};
            for(const key in ref){
                obj[key] = refs.indexOf(ref[key]);
            }

            return [TYPE.OBJECT,[ref.constructor.name,obj]];
        }

        return [TYPE.VALUE,ref];
    });

    const obj = [refs.indexOf(entry),data];
    if (type === EXPORT.OBJECT){
        return obj as Serialized;
    }

    const json = JSON.stringify(obj);
    if (type === EXPORT.JSON){
        return json;
    }

    if (type === EXPORT.BASE64){
        return Buffer.from(json).toString('base64');
    }

    throw new Error("Unknown export type `"+JSON.stringify(type)+"`");
};

export default serialise;