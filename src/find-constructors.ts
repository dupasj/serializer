const fetchConstructors = (entry: any) => {
    const constructors: {new (): any,name: string}[] = [];

    const done: any[] = [];
    const traverse = (test: any) => {
        if (done.includes(test)){
            return;
        }

        done.push(test);

        if (Array.isArray(test)){
            test.forEach(traverse);
            return;
        }

        if (typeof test !== "object" || test === null){
            return;
        }

        if (!constructors.includes(test.constructor)){
            constructors.push(test.constructor);
        }
        for(const key in test){
            traverse(test[key]);
        }
    }

    traverse(entry);

    return constructors;
};


export default fetchConstructors;
