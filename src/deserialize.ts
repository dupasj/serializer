import TYPE from "./type/type";
import Serialized from "./type/serialized";


const deserialize = <T extends any = any>(serialized: string|Serialized, constructors: {new (): any,name: string}[] = []): T => {
    if (typeof serialized === "string"){
        try{
            const str = Buffer.from(serialized,"base64").toString("utf-8");

            return deserialize<T>(JSON.parse(str) as Serialized,constructors);
        }catch (e){
            // Failed to parse serialized string, try without decoding base64 string
            try{
                return deserialize<T>(JSON.parse(serialized) as Serialized,constructors);
            }catch (e){
                throw new Error("Failed to convert your serialized string.");
            }
        }
    }

    if (!Array.isArray(serialized) || serialized.length !== 2 || typeof serialized[0] !== "number" && !Array.isArray(serialized[1])){
        throw new Error("Your serialized data does not match with our expected data serialization value. See Serialized type.");
    }

    const refs = serialized[1];
    const done = new Map<number,any>();
    const buildFromRef = (id: number) => {
        if (done.has(id)){
            return done.get(id);
        }

        if (!(id in refs)){
            throw new Error("The reference `"+id+"` is unknown from your refs data from your serialized data");
        }

        const ref = refs[id];

        if (!Array.isArray(ref) || ref.length < 1 || typeof ref[0] !== "number"){
            throw new Error("The reference `"+id+"` from your serialized data does not match with our expected data serialization value. See SerializedValue type.");
        }

        if (ref[0] === TYPE.VALUE){
            if (ref.length < 2 || typeof ref[1] === "undefined" || Array.isArray(ref[1]) || (ref[1] !== null && typeof ref[1] === "object")){
                throw new Error("The reference `"+id+"` from your serialized data does not match with our expected data serialization value. See SerializedValue type.");
            }

            const value = ref[1];
            done.set(id,value);

            return value;
        }
        if (ref[0] === TYPE.ARRAY){
            const value: any[] = [];
            done.set(id,value);

            if (!Array.isArray(ref[1])){
                throw new Error("The reference `"+id+"` from your serialized data does not match with our expected data serialization value. See SerializedArrayValue type.");
            }

            value.push(... ref[1].map(buildFromRef))
            return value;
        }

        if (ref[0] === TYPE.OBJECT){
            const obj = ref[1][1];

            if (!Array.isArray(ref[1]) || ref[1].length !== 2 || typeof ref[1][0] !== "string" || typeof ref[1][1] !== "object" || ref[1][1] === null){
                throw new Error("The reference `"+id+"` from your serialized data does not match with our expected data serialization value. See SerializedArrayValue type.");
            }

            const constructor = constructors.find(i => {
                return i.name === ref[1][0];
            }) || Object;

            const value = new constructor;
            done.set(id,value);

            for(const key in obj){
                value[key] = buildFromRef(obj[key]);
            }

            return value;
        }

        if (ref[0] === TYPE.UNDEFINED){
            done.set(id,undefined);

            return undefined;
        }

        throw new Error("You reference data type `"+ref[0]+"` is unknown from our serialized logic");
    };

    for(let i=0;i<refs.length;i++){
        buildFromRef(i);
    }

    if (!done.has(serialized[0])){
        throw new Error("The reference `"+serialized[0]+"` is unknown from your refs data from your serialized data");
    }

    return done.get(serialized[0]);
};

export default deserialize;