import SerializedArrayValue from "./serialized-value-array";
import SerializedObjectValue from "./serialized-value-object";
import SerializedRawValue from "./serialized-value-raw";
import TYPE from "./type";

type SerializedValue = SerializedArrayValue|SerializedObjectValue|SerializedRawValue|[TYPE.UNDEFINED];

export default SerializedValue;