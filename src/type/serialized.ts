import SerializedValue from "./serialized-value";

type Serialized = [
    number,
    SerializedValue[]
]

export default Serialized;