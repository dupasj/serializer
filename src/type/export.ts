enum EXPORT {
    OBJECT = "object",
    JSON = "json",
    BASE64 = "base64"
}

export default EXPORT;