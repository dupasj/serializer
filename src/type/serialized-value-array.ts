import TYPE from "./type";

type SerializedArrayValue = [
    TYPE.ARRAY,
    number[]
];

export default SerializedArrayValue;