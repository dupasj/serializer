enum TYPE {
    OBJECT,
    VALUE,
    ARRAY,
    UNDEFINED,
}

export default TYPE;