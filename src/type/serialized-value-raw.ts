import TYPE from "./type";

type SerializedRawValue = [
    TYPE.VALUE,
    null|number|string|boolean
];

export default SerializedRawValue;