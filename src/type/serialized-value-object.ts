import TYPE from "./type";

type SerializedObjectValue = [
    TYPE.OBJECT,
    [
        string,
        {[key: string]: number}
    ]
];

export default SerializedObjectValue;