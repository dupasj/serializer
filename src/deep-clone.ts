import {deserialize, serialize} from "./index";
import EXPORT from "./type/export";
import fetchConstructors from "./find-constructors";

const deepClone = (entry) => {
    return deserialize(serialize(entry,EXPORT.OBJECT),fetchConstructors(entry));
}

export default deepClone;