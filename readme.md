# Why should I use it ?
- Thus functions allow us the build the classes who have been serialized when we want to deserialize them. 
- More of that, the serialize function can return a native javascript object (usefull if you want to sign it in a [json web token][https://www.npmjs.com/package/jsonwebtoken]). 
- The serialize data keeps the circular object (we can use it if you want to output json data in our request and easily keep the circular data in the client)
# How to use it ?
Here is a short example with the following variables:
```
class User{
    lastname: string|null = null;
    firstname: string|null = null;
    slug: string|null = null;
    life: string|undefined = undefined;
    french: boolean = true;
    emails: Email[] = [];
}
const user = new User();
user.lastname = "dupas";
user.firstname = "jérémie";

class Email{
    user: User|null = null;
    value: string|null = null;
    getUser(){
        return this.user;
    }
}

const email = new Email();
email.value = "dupasj97@gmail.com";
email.user = user;
user.emails = [email];
```
Now let's try to serialize the following data:
````
import {serialize} from "serializer/serialize";
const serialized = serialize(user);
````

By default, the serialize function return a base64 string. But you can change the type of export with the second parameter:
````
import EXPORT from "serializer/type/export";

// Export serialized data as base64 data
const base64 = serialize(user,EXPORT.BASE64); // or serialize(user,"base64");

// Export serialized data as json data
const json = serialize(user,EXPORT.JSON); // or serialize(user,"json");

// Export serialized data as javascript object data
const obj = serialize(user,EXPORT.OBJECT); // or serialize(user,"object");
````

Now you can decode the serialized data with the following method:
````
import deserialize from "serializer/deserialize";

const clone = deserialize(serialized);


// No matter witch type of export you have been done, the function will notice it.
const clonedFromBase64 = deserialize(base64);
const clonedFromJson = deserialize(json);
const clonedFromObj = deserialize(obj);
````
All the `console.log` for each result will return: 
```
{
  lastname: 'dupas',
  firstname: 'jérémie',
  slug: null,
  life: undefined,
  french: true,
  emails: [ { user: [Circular], value: 'dupasj97@gmail.com' } ]
}
```

For now, the deserialize data didn't build our classes (he constructs the object). To build with own classes, we have to list them in the second parameter:
```
const clone = deserialize(serialized,[User,Email]);
```
And now the `console.log` of the result is:
```
User {
  lastname: 'dupas',
  firstname: 'jérémie',
  slug: null,
  life: undefined,
  french: true,
  emails: [ Email { user: [Circular], value: 'dupasj97@gmail.com' } ]
}
```


## Usefull
We can usually fetch all used constructor in your variable with the following function:
````
import findConstructors from "serializer/find-constructor";
const constructors = findConstructors(user);
````

You can usually deep clone your variable with the following function:
```
import deepClone from "serializer/deep-clone";
const deepClone = deepClone(user);
````

If you use typescript, you can type the deserialized data like this:
```
const clone = deserialize<User>(serialized,[User,Email]);
```

[https://www.npmjs.com/package/jsonwebtoken]: https://www.npmjs.com/package/jsonwebtoken

# Warning
Thus functions do not serialized function value type for security reason.